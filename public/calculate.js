function calculatePercent(sender)
{
   var formid = sender.parentNode.id;
   var elements = document.getElementById(formid).childNodes;
   var values = [elements[1].value, elements[3].value];

   var percentage = Math.round(values[0]/values[1] * 100);
   var text;
   if (!isFinite(percentage)) {
       text = "";
   }
    else {
        text = percentage + "%";
    }
   document.getElementById("activity" + formid[8] + "total").innerHTML = text;

}

function calculateMean()
{
    var scores = [];
    for (i = 1; i <= 4; i++) {
        var val = document.getElementById("activity"+i+"score").value;
        if (val != null && val != "") {
            scores.push(val);
        }
    }
    
    var totals = [];
    for (i = 1; i <= 4; i++) {
        var val = document.getElementById("activity"+i+"max").value;
        if (val != null && val != "") {
            totals.push(val);
        }
    }

    if (scores.length != totals.length) {
        document.getElementById("total").innerHTML = "Invalid";
        return;
    }

    var total = 0;
    for (i = 0; i < scores.length; i++) {
        total = total +  (parseInt(scores[i]) / parseInt(totals[i]));
    }

    total = total/scores.length;
    total = Math.round(total * 1000) / 10;
    document.getElementById("total").innerHTML = total + "%";
}

function calculateAverage()
{
    var scores = [];
    for (i = 1; i <= 4; i++) {
        var val = document.getElementById("activity"+i+"score").value;
        if (val != null && val != "") {
            scores.push(val);
        }
    }
    
    var totals = [];
    for (i = 1; i <= 4; i++) {
        var val = document.getElementById("activity"+i+"max").value;
        if (val != null && val != "") {
            totals.push(val);
        }
    }

    if (scores.length != totals.length) {
        document.getElementById("total").innerHTML = "Invalid";
        return;
    }

    var scoreTotal = 0;
    var totalTotal = 0;
    for (i = 0; i < scores.length; i++) {
        scoreTotal += parseFloat(scores[i]);
        totalTotal += parseFloat(totals[i]);
    }

    var total = scoreTotal/totalTotal;

    total = Math.round(total * 1000) / 10;
    document.getElementById("total").innerHTML = total + "%";
    
    var gpa = 0;
    if (total < 50) {
        gpa = 0;
    } 
    else if (total < 55) {
        gpa = 1;
    }
    else if (total < 60) {
        gpa = 1.67;
    }
    else if (total < 65) {
        gpa = 2;
    }
    else if ( total < 70) {
        gpa = 2.33;
    }
    else if (total < 75) {
        gpa = 2.67;
    }
    else if (total < 80) {
        gpa = 3;
    }
    else if (total < 85) {
        gpa = 3.33;
    }
    else if (total < 90) {
        gpa = 3.67;
    }
    else if (total < 95) {
        gpa = 4;
    }
    else {
        gpa = 4.33;
    }

    document.getElementById("gpa").innerHTML = gpa;

}

function reset()
{
    for (i = 1; i <= 4; i++) {
        document.getElementById("activity"+i+"score").value = "";
        document.getElementById("activity"+i+"max").value = "";
        document.getElementById("activity"+i+"total").innerHTML = "";
    }

    document.getElementById("total").innerHTML = "";
    document.getElementById("gpa").innerHTML = "";
}
